FROM nginx as runtime

WORKDIR /usr/share/nginx/html

RUN rm -rf *

COPY dist/HWClienApp .

CMD ["nginx", "-g", "daemon off;"]
